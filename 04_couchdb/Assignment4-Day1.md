# Assignment 4 - Day 1

## Find

1. Find the CouchDB HTTP API reference documentation online. Give the URL.

```

```

2. We’ve already used GET, POST, PUT, and DELETE. What other HTTP methods are supported?

```

```

## Do

1. Use cURL to PUT a new document into the music database with a specific
_id of your choice. Give the command you ran.

```

```

2. Use cURL to create a new database with a name of your choice, and then
delete that database also via cURL. Give the commands you used.

```

```
