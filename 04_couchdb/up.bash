#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}"

>&2 echo "Starting CouchDB..."
docker compose --file docker-compose.yml up --detach

restapi=https://5984-${GITPOD_WORKSPACE_URL#https://}/
fouxton_login="https://5984-${GITPOD_WORKSPACE_URL#https://}/_utils/"

>&2 echo "
Fauxton:
    URL: $fouxton_login
    User: couchdb
    Password: bouchdc

REST-API:
    Run the following in your GitPod terminal:

        export COUCH_ROOT_URL=http://localhost:5984
    
    Now you can run the commands in your text. For nicer formatted output
    append '| jq' to each command. For example:

        curl \${COUCH_ROOT_URL} | jq
"