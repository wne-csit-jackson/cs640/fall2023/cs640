# Run this from the command-line like this
#    bash example-load-data.bash

# Define the location of CouchDB.
export COUCH_ROOT_URL=http://localhost:5984

# Create the music database.
curl -i -XPUT ${COUCH_ROOT_URL}/music

# Insert a row into the music database.
# Take not of the syntax to insert a single quote inside a multi-line
# single quoted string: '"'"' .
curl -i -XPOST "${COUCH_ROOT_URL}/music/" \
    -H "Content-Type: application/json" \
    -d '{
        "name": "The Beatles",
        "albums": [
            {
                "title": "Help!",
                "year": 1965
            },
            {
                "title": "Sgt. Pepper'"'"'s Lonely Hearts Club Band",
                "year": 1967
            },
            {
                "title": "Abbey Road",
                "year": 1969
            }
        ]
    }'
