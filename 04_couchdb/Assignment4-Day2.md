# Assignment 4 - Day 2

## Find

1. We’ve seen that the emit() method can output keys that are strings. What
other types of values does it support? What happens when you emit an
array of values as a key?

```

```

2. Find a list of available URL parameters (like limit and startkey) that can be
appended to view requests and what they do. Give the URL below.

```

```

## Do

1. The import script import_from_jamendo.rb assigned a random number to each
artist by adding a property called random (tip: `rand` calls the random
function, which returns a returns a random number). Create a mapper function that
will emit key-value pairs where the key is the random number and the
value is the band’s name. Save this in a new design document named
_design/random with the index name artist.

Write your mapper below.

```

```

2. Craft a cURL request that will retrieve a random artist.
Hint: You’ll need to use the startkey parameter, and you can produce a random
number on the command line using `ruby -e 'puts rand'`.

```

```


3. (OPTIONAL; but good practice) The import script also added a random property
for each album, track, and tag. Create three additional views in the
_design/random design document with the index names album, track, and tag to
match the earlier artist view.

Provide mapping functions below.

```

```
