# Assignment 5 - Day2

Work your way through the examples in Day 2 up to but not including "Big Data".
Do so by building a file named `day2.bash`. I have started it for you.
Follow the format demonstrated in this file, including comments.

By the end, it should run all the examples (adjusted appropriately for
your data).

Feel free to create more nodes to make the "Finding Your Path" section more
interesting.
