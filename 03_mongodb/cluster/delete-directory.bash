#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}"

if [ $# -lt 1 ] || [ -z "$1" ] ; then
    >&2 echo "usage: $0 DIRECTORY_NAME"
    exit 1
fi

sudo rm -rf "$1"
