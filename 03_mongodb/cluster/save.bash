#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}"

set -euo pipefail

if [ $# -lt 1 ] || [ -z "$1" ] ; then
    >&2 echo "usage: $0 NEW_DIRECTORY_NAME"
    exit 1
fi

if docker ps -f name=7dbs_mongo_db | grep 7dbs_mongo_db > /dev/null ; then
    >&2 echo "Please stop the DB first."
    exit 1
fi

size=$(sudo du -s --block-size=MB ./.data/ | tail -n 1)
size=${size%MB*}
THRESHOLD=500
if [[ $size -gt $THRESHOLD ]] ; then
    echo "./data/ is ${size}MB. It is bigger than ${THRESHOLD}MB."
    echo "We do not want to save this in our repository. Not saving."
    exit 1
fi

sudo rm -rf "$1"
sudo cp -R ./.data/ "$1"
