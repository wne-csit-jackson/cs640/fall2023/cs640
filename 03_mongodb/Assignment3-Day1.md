# Assignment 3 - Mongo Day 1

## Day 1 Homework

Find

1. Bookmark the online MongoDB documentation and read up on something
you found intriguing today. Provide the URL.

2. Look up how to construct regular expressions in Mongo. Provide the URL.

3. Acquaint yourself with command-line db.help() and db.collections.help() output.
    Nothing to provide.

4. Find a Mongo driver in your programming language of choice (Ruby, Java,
PHP, Go, Elixir, and so on). Provide URL to list of drivers/libraries for
different languages.


Do

1. Print a JSON document containing { "hello" : "world" }.
    Remember, the mongo shell is JavaScript.
    Write the command. (Tip: use JSON.stringify)

2. Select a town via a case-insensitive regular expression containing the
word new. (Tip: append .pretty() to your command to get nicer output).
Write the command.

3. Find all cities whose names contain an e and are famous for food or beer.
Write the command.

4. Create a new database named blogger with a collection named articles. Insert
a new article with an author name and email, creation date, and text.
Write the command.

5. Update the article with an array of comments, containing a comment with
an author and text.
Write the command.

6. Run a query from an external JavaScript file that you create yourself.
