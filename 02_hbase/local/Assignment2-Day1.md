# Assignment 2 - Day 1

This is the first part of Assignment 2. There may be more. You instructor
will let you know.

## Day 1 Homework

Find

1. Write commands here.

2. Write URL here.

Do

1. This is the same question as the one from the text, but I have given
   you a fair amount of help. The code fragment below was created by combining
   the example on p65 with the starter function in the problem description.
   The fragment contains additional tips to help you.

    def put_many(table_name, row, column_values)
        # The code in this function was taken from the example on pg 65.
        # You need to generalize it by replacing strings like "wiki" and
        # "Home" with parameter variables.

        table = HTable.new(@hbase.configuration, "wiki")
        p = Put.new(*jbytes("Home"))

        # The following three lines from the example on pg 65 must be
        # converted into a loop.
        #
        #    p.add(*jbytes("text", "", "Hello world"))
        #    p.add(*jbytes("revision", "author", "jimbo"))
        #    p.add(*jbytes("revision", "comment", "my first edit"))
        #
        # The column_values parameter contains a hash. See the second problem
        # in the text for an example of what will be passed for column_values.
        #
        # In Ruby, you use the "each_pair do" to loop through the values of a hash.
        # For example, if myhash is a variable containing a hash, here is
        # a loop that makes each key, value pair available inside the loop.
        #
        #     myhash.each_pair do |key, value|
        #         # key and value will be contain a different key and value
        #         # on each pass of the loop.
        #     end
        #
        # You'll need one more trick to complete the loop. Notice that each
        # key contains a string that has the form "family:column". You'll
        # need to use the split function to separate them into two variables.
        # Here is the code for that.
        #
        #     family, column = key.split(':')
        #
        # Now write a loop to process each key, value pair in column_values
        # and add them to the Put object p.


        # Now we execute the put statement. You don't need to modify this
        # statement.
        table.put(p)
    end
    
2. Run the command given in the problem, then run

        scan "wiki"
    
    Paste the output below.


