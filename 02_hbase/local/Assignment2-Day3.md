# Assignment 2 - Day 3 and CAP (Appendix 2)

1. List the pros of HBase as described in our text.

2. List the cons of HBase as described in our text.

3. List and define the CAP properties.

4. Classify the following fictitious database in terms of CAP.

    UberDB is a distributed database system. When there is a network
    failure, individual nodes continue to operate accepting reads and writes.
    When the network is restored, a reconciliation process is used to determine
    the new current state of the database system.

5. Classify the following fictitious database in terms of CAP.

    UltraDB is a distributed database system. When there is a network
    failure, individual nodes continue to respond to read operations,
    but write operations are prevented (an error is reported). When the
    network is restored, the database system resumes normal operation.

6.  Classify the following fictitious database in terms of CAP.

    NeatoDB is a network accessible database that runs on a single machine.
    When there is a network failure, the database continues to operate as
    normal, of course it cannot serve requests from the network.
