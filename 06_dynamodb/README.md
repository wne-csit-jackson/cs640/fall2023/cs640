
## Start the database

```bash
./up.bash
```

## Install aws into your bash terminal

```bash
source aws-init.bash
```

You can now run aws commands in your terminal as shown in the text.

## Stop and delete the database

```bash
./down.bash
```
