aws() {
    docker run \
    --rm \
    -it \
    --name 7dbs_dynamo_cli \
    --network dynamo_net \
    -e "AWS_ACCESS_KEY_ID=dummy" \
    -e "AWS_SECRET_ACCESS_KEY=dummy" \
    -e "AWS_DEFAULT_REGION=eu-west-1" \
    -e "AWS_DEFAULT_OUTPUT=json" \
    amazon/aws-cli \
    "$@" --endpoint-url http://db:8000
}