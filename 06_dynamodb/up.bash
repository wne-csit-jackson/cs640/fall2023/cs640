#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}"

>&2 echo "Starting DynamoDB (local)..."
docker network create -d bridge --subnet 173.106.0.0/24 dynamo_net

docker compose up --detach

>&2 echo "Install the aws command into your shell as follows:"
>&2 echo
>&2 echo "      source aws-init.bash"
>&2 echo
