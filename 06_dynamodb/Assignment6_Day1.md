# Assignment 6 - Day1

## Find

1. DynamoDB does have a specific formula that’s used to calculate the
number of partitions for a table. Do some Googling and find that formula.
Provide the formula below.

```

```


2. Browse the documentation for the DynamoDB streams feature. The link
    is given in the footnotes of the text. (No answer necessary here.)


3. We mentioned limits for things like item size (400 KB per item). Read the
    Limits in DynamoDB documentation to see which other limitations apply
    when using DynamoDB so that you don’t unknowingly overstep any
    bounds. The link is given in the footnotes of the text.
    (No answer necessary here.)


Do


1. Using the formula you found for calculating the number of partitions used
    for a table, calculate how many partitions would be used for a table
    holding 100 GB of data and assigned 2000 RCUs and 3000 WCUs.

```

```

2. If you were storing tweets in DynamoDB, how would you do so using
DynamoDB’s supported data types? A rough sketch is fine.

```

```

3. In addition to PutItem operations, DynamoDB also offers update item operations
    that enable you to modify an item if a conditional expression is satisfied.
    Take a look at the documentation for conditional expressions (the link is provided
    in the footnotes of the text) and perform a conditional update operation on an
    item in the ShoppingCart table. Provide the command below.

```

```