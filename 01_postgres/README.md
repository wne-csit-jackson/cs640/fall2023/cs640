# 01_postgres

This file describes how to manipulate the
Postgres server so that you can work through the
Postgres chapters in our text.

## Starting and connecting to the Postgres server

In GitPod...

1. Open a terminal

2. Change to the 01_postgres directory

    ```bash
    cd 01_postgres
    ```

3. Start the Postgres server

    ```bash
    ./up.bash
    ```

4. Connect to the Postgres server

    ```bash
    ./shell.bash
    ```

Your prompt should be someting like `7dbs=# `.
You are now ready to start ***AFTER*** `$ psql 7dbs` in the text at the
bottom of page 11.

When you are done interacting with the Postgres server, type `exit` or `\q`
to return to the bash prompt. This does not stop the server, it just
disconnects you from the server. You can reconnect by running `./shell.bash`
again.

## Saving the database state

Assuming your Postgres server is running...

In a bash terminal positioned in the `01_postgres/` directory...

```bash
./save.bash data.sql
```

This dumps the database into a file named `data.sql`.


## Stopping and delete the Postgres server

```bash
./down.bash
```

## Loading the database state

After starting a new Postgres server, it will be
empty. Assuming you saved the state of your database
in `data.sql`, you can restore the currently empty
server to this state as follows.

```bash
./load.bash data.sql
```
