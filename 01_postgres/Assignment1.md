
# Assignment 1 - Postres

The deadline for this assignment is in Kodiak.

Work through the reading in Chapter 2 (Day 1 through Day 3) and complete
the in chapter questions as directed below. ***Save your resulting database
in a file named `db.sql` in the same directory as this file.***

## Day 1 Homework

**Find**

1.

2. (No answer required)

3.


**Do**

1. (Write the SQL query you created.)

2. (Write the SQL query you created.)

3. (Write the SQL querey you created.)


## Day 2 Homework

**Find**

1. (Provide a link)

2. (Provide a link)


**Do**

1. (Write the SQL query you created.)

2. (Write the SQL query you created.)

3. (Write the SQL querey you created.)


## Day 3

Read ONLY the Wrap-Up. Do not complete the in-chapter homework.


1. To ease studying, write a brief list of Postgres' strengths.


2. To ease studying, write a brief list of Postgres' weaknesses.

## Submission

Use bin/synch.bash to commit your work to your repository by the deadline.
